function degToRad(deg)
{
    return deg * Math.PI / 180;
}
function rotateWorld(mesh, axis, radians)
{
    rotWorldMatrix = new THREE.Matrix4();
    rotWorldMatrix.makeRotationAxis(axis, radians);
    rotWorldMatrix.multiply(mesh.matrix);  // pre-multiply
    mesh.matrix = rotWorldMatrix;
    mesh.setRotationFromMatrix(mesh.matrix);
}



