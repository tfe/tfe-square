// we are in ux environnement here
(function(ux)
{
    var square = function()
    {
        var player = this;

        this.z=0;
        this.original_speed = 10;
        this.speed = this.original_speed;
        this.size = 10;
        this.moving= null;
        this.selected = true;

            // Data to know how to move the cube, depending on the wanted rotation
        this.directions= {
                'top' : { 'direction':'top','move': 'y', 'rotate': 'x', 'rotate_inc':-1, 'increment': 1 },
                'bottom' : { 'direction':'bottom','move': 'y', 'rotate': 'x', 'rotate_inc':1, 'increment': -1 },
                'left' : { 'direction':'left','move': 'x', 'rotate': 'y', 'rotate_inc':-1, 'increment': -1 },
                'right' : { 'direction':'right','move': 'x', 'rotate': 'y', 'rotate_inc':1, 'increment': 1 },
        };

        this.coords = {
            x:{ coord: 'x', delta: 1 },
            y:{ coord: 'y', delta: 1 },
            z:{ coord: 'z', delta: 1 }
        };

            // Init the cube in the scene
        this.init= function(params)
        {
            this.id = Math.abs(Math.random() * 100000);
            this.x = params.x;
            this.y = params.y;
            if(params.size)
            {
                this.size = params.size;
            }
            if(params.speed)
            {
                this.speed = params.speed;
            }

            var id = Math.abs(Math.random() * 100);
            var container = ux.add({ "command": "add", "id":"container"+id,"type":"object", "position": [ this.x*this.size+this.size/2, this.y*this.size+this.size/2, this.size/2], "rotation":[0,0,0]  });
            var mesh = ux.add({ "command": "add", "id":"player"+id, "parent": "container"+id, "type":"object", "position": [ 0,0 ,0], "rotation":[0,0,0], "size": this.size  });
            var cube = ux.add(
                {
                    "command": "add",
                    "id":"player_mesh"+id,
                    "type":"cube",
                    "parent": "player"+id,
                    "position": [0,0,0],
                    "rotation":[0,0,0],
                    "size": this.size,
                    "materials": params.materials
                });
            var boundbox = ux.add(
                {
                    "command": "add",
                    "id":"player_bound"+id,
                    "type":"plane",
                    "parent": "container"+id,
                    "position": [0,0,this.size/2-0.1],
                    "rotation":[0,0,0],
                    "width": this.size,
                    "height": this.size,
                    "material": { opacity: 0}
                });
            this.arrow = ux.add(
            {
                "command": "add",
                "id":"arrow"+id,
                "type":"gltf",
                "parent": "container"+id,
                "position": [-this.size/2,this.size/2,this.size/2],
                "material": { opacity: 0},
                "rotation":[50,20,30],
                "scale": [4, 4, 4],
                "href": "objects/arrow.glb"
            });
            this.cube = cube;
            this.boundbox = boundbox;
            this.mesh = mesh;
            this.container = container;

            // DEBUG PURPOSE
            if(!ux.player)
            {
                ux.player=this;
            }
        };

        this.enable_selected = () =>
        {
            this.arrow.children.forEach(function(child)
            {
                child.material.opacity=1.0;
            });
            ux.animate('player_selected'+this.id, this.selected_anim.bind(this));
            this.selected = true;
        }

        this.disable_selected = () =>
        {
            this.selected = false;
        }

        this.selected_anim = () =>
        {
            if(!this.selected) {
                // Reset colors and stop animation
                this.arrow.children.forEach(function(child)
                {
                    child.material.color = new THREE.Color(0,0,0);
                    child.material.opacity=0;
                });
                return false;
            }
            this.arrow.children.forEach(function(child)
            {
                child.material.opacity=Math.sin(new Date().getTime()*0.0040)*0.2+0.7;
            });
            return true;
        };

        // Trigger move: launch the move animation
        this.trigger_move = (direction,callback) =>
        {
            // Do not allow multiple moves
            if(this.moving) {  return false; }
            this.step=0;

            var d =  Object.assign({}, this.directions[direction]);
            var orig_rotate = d.rotate;

            var q = new THREE.Quaternion();
            q.copy(this.container.quaternion);
            this.moving =
            {
                callback: callback,
                direction: direction,
                before_rotation_quaternion: q,
                data: d
            };

            // Tell ux to do the animation of the move
            ux.animate('player_move'+this.id, this.move.bind(this));
            return true;
        }

        // Move steps, animation
        this.move = (delta) => 
        {
            if(this.moving)
            {
                this.speed = this.original_speed * delta * 30;
                var direction = this.moving.direction;
                this.step += this.speed;
                if(this.step>100) { this.step =  100; }
                var step = this.step/100;
                var speed = this.speed/100;

                var d = this.moving.data;

                this.mesh.rotation[d.rotate] =  degToRad(d.rotate_inc*90 * step);

                // Position incremented manually
                this.container.position[d.move] += (d.increment)*this.size*speed;

                // bounce
                if(step<0.5)
                {
                    this.mesh.position.z  = (step * Math.sqrt(2*Math.pow(this.size/2,1.5)));
                }
                else
                {
                    this.mesh.position.z  = ((0.5-step+0.5) * Math.sqrt(2*Math.pow(this.size/2,1.5)));
                }

                if(this.step>=100)
                {

                    this.step=0;
                    // Save new player position and angle
                    player[d.move] += d.increment;

                    var look_destination = new THREE.Vector3(0, 0 ,0);
                    look_destination[d.move] += d.increment;

                    // Fix last positions
                    this.mesh.rotation[d.rotate] =  0;
                    this.cube.geometry.lookAt(look_destination);
                    //
                    this.container.position.x = this.x*this.size+this.size/2;
                    this.container.position.y = this.y*this.size+this.size/2;

                    // Update coords
                    var old_coords = Object.assign({} , this.coords);
                    if(direction=='left')
                    {
                        this.coords.x = { coord: old_coords.z.coord, delta: -old_coords.z.delta};
                        this.coords.z = { coord: old_coords.x.coord, delta: old_coords.x.delta};
                    }
                    if(direction=='right')
                    {
                        this.coords.x = { coord: old_coords.z.coord, delta: old_coords.z.delta};
                        this.coords.z = { coord: old_coords.x.coord, delta: -old_coords.x.delta};
                    }
                    if(direction=='top')
                    {
                        this.coords.y = { coord: old_coords.z.coord, delta: old_coords.z.delta};
                        this.coords.z = { coord: old_coords.y.coord, delta: -old_coords.y.delta};
                    }
                    if(direction=='bottom')
                    {
                        this.coords.y = { coord: old_coords.z.coord, delta: -old_coords.z.delta};
                        this.coords.z = { coord: old_coords.y.coord, delta: old_coords.y.delta};
                    }

                    if(this.moving.callback)
                    {
                        this.moving.callback();
                    }
                    // Reset moving info
                    this.moving=null;
                    return false;
                }
                return true;
            }
            return false;
        };
        return this;
    };

    window.square = square;
})(this);


