game.ux.add(
    { "command": "add", "id":"pingu","type":"gltf", "position": [ 0, 0, 4], "scale": [20,20,20] , "href": "objects/pingu_with_armature.glb" }
);
game.ux.animate(
    { "command": "animate", "function": "if(this.ids['pingu']) this.ids['pingu'].rotation.z += 0.01; " }
);
