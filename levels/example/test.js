[
    { "command": "log", "message":"Connected" },
    { "command": "init" },

    { "command": "add", "id":"scene", "type":"scene" },
    { "command": "add", "id": "camera", "type":"camera", "position": [100,0,110], "rotation":[0,45,90] },

    { "command": "add", "id":"floor","type":"plane", "position": [ 0,0,0],  "rotation":[0,0,0], "width":100, "height":100 },
    { "command": "add", "id":"floorgrid","type":"grid", "position": [ 0,0,0], "rotation":[90,0,0], "divisions": 10,  "size":100, "color": "#000000" },

    { "command": "add", "id":"first","type":"plane", "position": [ 25,-25,10],  "rotation":[0,0,0], "width":50, "height":50 },
    { "command": "add", "id":"floorgrid","type":"grid", "position": [ 25,-25,10], "rotation":[90,0,0], "divisions": 5,  "size":50, "color": "#000000" },

    { "command": "loadjs", "file":"js/characters/pingu.js"}



]
