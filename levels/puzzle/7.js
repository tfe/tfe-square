// we are in ux environnement here
(function(ux)
{
    ux.load_game(
    {
        "playable": true,
        "image":"levels/puzzle/images/tfe.jpg",
        "image_items":"1",
        "best_moves": 10,
        "map":
        {
            "width": 8,
            "height": 5,
            "invalid_positions":
            [
                { "x": 6, "y": 5 },
                { "x": 6, "y": 4 },
                { "x": 6, "y": 3 },
                { "x": 6, "y": 1 },
                { "x": 6, "y": 0 },
            ],
            // where the items are initially
            "items_data":
            [
                { "x": 1, "y": 0 },
            ],
            // Where the puzzle must be solved
            "destination": { "x" : 7, "y": 4, "rotation":0  }
        }
    });
})(this);


