// we are in ux environnement here
(function(ux)
{
    ux.load_game(
    {
        "playable": true,
        "image":"levels/puzzle/images/tfe.jpg",
        "image_items":"1",
        "best_moves": 8,
        "map":
        {
            "width": 7,
            "height": 4,
            "invalid_positions":
            [
                { "x": 0, "y": 3},
                { "x": 1, "y": 0},
                { "x": 1, "y": 1},
                { "x": 1, "y": 3},
                { "x": 2, "y": 0},
                { "x": 2, "y": 3},
                { "x": 3, "y": 0},
                { "x": 3, "y": 2},
                { "x": 3, "y": 3},
                { "x": 4, "y": 0},
                { "x": 4, "y": 3},
                { "x": 5, "y": 0},
                { "x": 5, "y": 1},
                { "x": 5, "y": 2},
                { "x": 5, "y": 3},
            ],
            // where the items are initially
            "items_data":
            [
                { "x": 0, "y": 0 }, // First item, bottom left
            ],
            // Where the puzzle must be solved
            "destination": { "x" : 4, "y": 2, "rotation": 0  }
        }
    });
})(this);


