// we are in ux environnement here
(function(ux)
{
    ux.load_game(
    {
        "playable": true,
        "image":"levels/puzzle/images/tfe.jpg",
        "image_items":"1",
        "best_moves": 4,
        "map":
        {
            "width": 5,
            "height": 1,
            // where the items are initially
            "items_data":
            [
                { "x": 0, "y": 0 }, // First item, bottom left
            ],
            // Where the puzzle must be solved
            "destination": { "x" : 4, "y": 0  }
        }
    });
})(this);


