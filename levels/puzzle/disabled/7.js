// we are in ux environnement here
(function(ux)
{
    ux.load_game(
    {
        "playable": true,
        "image":"levels/puzzle/images/tfe.jpg",
        "image_items":"2",
        "best_moves": 31,
        "map":
        {
            "width": 7,
            "height": 7,
            "invalid_positions":
            [
            ],
            // where the items are initially
            "items_data":
            [
                { "x": 1, "y": 0 },
                { "x": 2, "y": 0 },
                { "x": 1, "y": 1 },
                { "x": 2, "y": 1, "selected":true },
            ],
            // Where the puzzle must be solved
            "destination": { "x" : 5, "y": 4, "rotation":0  }
        }
    });
})(this);


