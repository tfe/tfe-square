// we are in ux environnement here
(function(ux)
{
    ux.load_game(
    {
        "playable": true,
        "image":"levels/puzzle/images/tfe.jpg",
        "image_items":"3",
        "best_moves": 8,
        "map":
        {
            "width": 10,
            "height": 10,
            "invalid_positions":
            [
                { "x": 3, "y": 4 },
                { "x": 5, "y": 4 },
                { "x": 7, "y": 4 }
            ],
            // where the items are initially
            "items_data":
            [
                { "x": 4, "y": 0, "playable": false },
                { "x": 5, "y": 0, "playable": false },
                { "x": 6, "y": 0, "playable": false },
                { "x": 4, "y": 1, "playable": false },
                { "x": 5, "y": 1, "playable": false },
                { "x": 6, "y": 1, "playable": false },
                { "x": 4, "y": 2, "playable": false },
                { "x": 5, "y": 6, "selected": true },
                { "x": 6, "y": 2, "playable": false },
            ],
            // Where the puzzle must be solved
            "destination": { "x" : 4, "y": 0, "rotation":0  }
        }
    });
})(this);


