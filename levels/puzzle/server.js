module.exports = function(connections, connection)
{
    var fs = require('fs'), path = require('path');

    this.status_msg = function()
    {
        return { command: 'status', players: Object.keys(connections).length  };
    };

    this.loaded = function()
    {
        console.log('received loaded!');
    };

    this.send = function(data)
    {
        connection.send(JSON.stringify(data));
    }

    this.connected= function(msg)
    {
        console.log('connected',msg);
        var status_msg = this.status_msg();
        this.tellall(status_msg);
    };

    this.disconnected= function(msg)
    {
        console.log('disconnected',msg);
        var status_msg = this.status_msg();
        this.tellall(status_msg);
    };

    this.start_level = function(msg)
    {
        var level = msg.level;
        var command = { "command":"loadjs"};
        if(!/^\d{1,3}$/.test(level))
        {
            command.command='error';
            connection.send(JSON.stringify(command));
        }
        var filePath = 'levels/puzzle/'+level+'.js';
        if(fs.existsSync(filePath))
        {
            var data  = fs.readFileSync(filePath);
            command.data = data.toString();
            connection.send(JSON.stringify(command));
        }
        else
        {
            command.command='error';
            command.data='FAIL LOAD FILE';
            connection.send(JSON.stringify(command));
        }

    };

    this.tellall = function(msg)
    {
        var msg = JSON.stringify(msg);
        for(conid in connections)
        {
            console.log('will send ',msg);
            connections[conid].send(msg);
        }
    };
};
